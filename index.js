const express = require('express');
const cors = require('cors');
const formData = require("express-form-data");

const {Book} = require('./models/');

const stor = {
  books: []
};

const app = express();

app.use(formData.parse());
app.use(cors());

// authorization
app.post('/api/user/login', (req, res) => {
  res.status(201);
  res.json({ id: 1, mail: "test@mail.ru" });
});


// get all books
app.get('/api/books/', (req, res) => {
  const {books} = stor;
  res.json(books);
});

// get book
app.get('/api/books/:id', (req, res) => {
  const {books} = stor;
  const {id} = req.params;
  const book = books.find(book => book.id === id);

  if (book) {
    res.json(book);
  } else {
    res.status(404);
    res.json("book | not found");
  }
});

// create book
app.post('/api/books/', (req, res) => {
  const {books} = stor;
  const {
    title,
    description,
    authors,
    favorite,
    fileCover,
    fileName
  } = req.body;

  const newBook = new Book(title, description, authors, favorite, fileCover, fileName);
  books.push(newBook);

  res.status(201);
  res.json(newBook);
});

// edit book
app.put('/api/books/:id', (req, res) => {
  const {books} = stor;
  const {
    title,
    description,
    authors,
    favorite,
    fileCover,
    fileName
  } = req.body;
  const {id} = req.params;
  const bookIndex = books.findIndex(book => book.id === id);

  if (bookIndex !== -1) {
    books[bookIndex] = {
      ...books[bookIndex],
      title,
      description,
      authors,
      favorite,
      fileCover,
      fileName
    };
    res.json(books[bookIndex]);
  } else {
    res.status(404);
    res.json("book | not found");
  }
});


// delete book
app.delete('/api/books/:id', (req, res) => {
  const {books} = stor;
  const {id} = req.params;
  const bookIndex = books.findIndex(book => book.id === id);

  if (bookIndex !== -1) {
      books.splice(bookIndex, 1);
      res.json(true);
  } else {
      res.status(404);
      res.json("book | not found");
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});